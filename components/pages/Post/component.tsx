import Head from 'next/head';
import { apiBaseUrl, controller } from '../../../core/api';
import HTMLReactParser from 'html-react-parser';

import {
  Heading,
  AnchorButton,
  Button,
  PostCard,
  PostCardAuthor,
  PostCardBody,
  PostCardCover,
  PostCardPublished,
  PostCover,
  Container,
  Grid,
  GridItem, TitleHeader, MainTemplate, PostBody,
} from '../../index';
import moment from 'moment';
import { GetServerSideProps } from 'next';
import { Props } from './props';
import React, { FC } from 'react';
import * as mock from '../../templates/MainTemplate/mock';
import locales from '../../../core/locales';
import { useRouter } from 'next/router';
import { useTheme } from '@emotion/react';

const formatDate = (date) => moment(String(date)).format("DD.MM.YYYY hh:mm");

const Post: FC<Props> = ({ post, posts, language }: Props) => {
  const theme = useTheme();
  const { push } = useRouter();
  if(!post && process.browser){
    push('/404')
  }
  const router = useRouter();


  return post ? (
    <div>
      <MainTemplate
        header={mock.header}
        footer={mock.footer}
      >
        <Head>
          <title>{post.title}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          <Container>
            <PostCover className="my-3" src={apiBaseUrl + post.cover.url} alt={post.title}/>
            <Heading as="h1" css={{ color: "#fff" }}>
              {post.title}
            </Heading>
            <div className="d-flex mt-3 mb-5">
              <PostCardAuthor href={post.author_link}
                              css={{marginRight: 10, fontWeight: 700, color: "#fff" }}>
                {post.author}
              </PostCardAuthor>
              <PostCardPublished css={{ color: "#fff" }}>{formatDate(post.published)}</PostCardPublished>
            </div>
            <PostBody className="ck-content" css={{ marginBottom: 100 }}>
              {HTMLReactParser(post.content)}
            </PostBody>


            <TitleHeader
                css={{ marginBottom: 20, background: theme.globalBg }}
                subtitle={
                  <Button className="py-2" css={{ fontSize: 12 }} onClick={() => router.push('/blog')}>{language.news.all}</Button>
                }
              >
                <Heading as="span" css={{ display: "block", fontSize: 20, fontWeight: 700 }}>{language.news.title}</Heading>
              </TitleHeader>
              <Grid columns={3} gap="20px">
                {posts.map((n, i) => (
                  <GridItem columns={1} key={i}>
                    <PostCard className="h-100">
                      <PostCardCover url={apiBaseUrl + n.cover.url}/>
                      <PostCardBody css={{ background: theme.globalBg }}>
                        <Heading className="mt-0" as="span" css={
                          {
                            textTransform: "uppercase",
                            display: "block",
                            fontSize: 17,
                            fontWeight: 700,
                            lineHeight: 1.6
                          }
                        }>
                          <AnchorButton
                            className="d-block"
                            css={{
                              color: 'inherit',
                              fontWeight: "inherit",
                              fontSize: "inherit"
                            }}
                            onClick={() => router.push(`/post/${n.id}`)}
                          >
                            {n.title}
                          </AnchorButton>
                        </Heading>
                        <div className="d-flex mt-3">
                          <PostCardAuthor href={n.author_link}
                                          css={{marginRight: 10, fontWeight: 700}}>
                            {n.author}
                          </PostCardAuthor>
                          <PostCardPublished>{formatDate(n.published)}</PostCardPublished>
                        </div>
                      </PostCardBody>
                    </PostCard>
                  </GridItem>
                ))}
              </Grid>
          </Container>
        </main>
      </MainTemplate>
    </div>
  ) : <div />;
}

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
  const language = ctx.locale || ctx.defaultLocale;
  const postId = ctx.query.id;
  const res = await controller.get(`/new/${postId}?lang=${language}`);
  const post = await res.data.body;

  const resNews = await controller.get(`/news/all?page=1&lang=${language}`);
  const posts = await resNews.data.body.slice(0, 3);

  if(post)
    return { props: { post, posts, language: locales[language] } }
  else {
    return {
      props: { post, posts, language: locales[language] },
    }
  }
}

export default Post;
