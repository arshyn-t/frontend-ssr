import { Post } from "../types/Post";
import locales from '../../../core/locales';

export type Props = {
  post?: Post,
  posts?: Post[],
  language: typeof locales,
}
