import { Props } from './props';

export { default, getServerSideProps } from './component';
export type PostProps = Props;
