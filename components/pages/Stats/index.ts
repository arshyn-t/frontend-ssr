import { Props } from './props';

export { default, getServerSideProps } from './component';
export type StatsProps = Props;
