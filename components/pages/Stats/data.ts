export const firstChart = (language) => ({
    labels: [ "2014 (1300)", "2015 (2500)", "2016 (1150)", "2017 (3200)", "2018 (90)", "2019 (61)"],
    datasets: [
        {
            label: language.graphs.terrorism,
            data: [474, 1056, 753, 2470, 48, 22],
            backgroundColor: "#f44336"
        },
        {
            label: language.graphs.social,
            data: [24, 412, 0, 36, 0, 0],
            backgroundColor: "#9C27B0"
        },
        {
            label: language.graphs.porn,
            data: [551, 722, 40, 508, 33, 5],
            backgroundColor: "#009688"
        },
        {
            label: language.graphs.drugs,
            data: [80, 155, 103, 46, 0, 3],
            backgroundColor: "orange"
        },
        {
            label: language.graphs.authority,
            data: [194, 34, 27, 0, 0, 14],
            backgroundColor: "#3F51B5"
        },
        {
            label: language.graphs.suicide,
            data: [23, 163, 18, 0],
            backgroundColor: "#2196F3"
        },
        {
            label: language.graphs.gaming,
            data: [0, 21, 213, 174, 16, 17],
            backgroundColor: "#ff85af"
        },
    ]
});

export const secondChart = (language) => ({
    labels: [ "2014 (33 адресов)", "2015 (35 адресов)"],
    datasets: [
        {
            label: language.graphs.terrorism,
            data: [33, 35],
            backgroundColor: "rgb(63, 81, 181)"
        }
    ]
});

export const thirdChart = (language) => ({
    labels: [ "2016 (более 31тыс.)", "2017 (более 10тыс.)", "2018 (более 9тыс.)", "2019 (21267)"],
    datasets: [
        {
            label: language.graphs.terrorism,
            data: [3155, 1958, 5000, 17000],
            backgroundColor: "#f44336"
        },
        {
            label: language.graphs.suicide,
            data: [25000, 5000, 1894, 2057],
            backgroundColor: "#9C27B0"
        },
        {
            label: language.graphs.drugs,
            data: [490, 2039, 855, 692],
            backgroundColor: "#009688"
        },
        {
            label: language.graphs.gaming,
            data: [448, 782, 427, 518],
            backgroundColor: "orange"
        },
        {
            label: language.graphs.privacy,
            data: [0, 0, 26, 2],
            backgroundColor: "#3F51B5"
        },
        {
            label: language.graphs.authority,
            data: [0, 0, 0, 46],
            backgroundColor: "#5ee364"
        },
        {
            label: language.graphs.health,
            data: [0, 0, 0, 58],
            backgroundColor: "#8a8a8a"
        },
        {
            label: language.graphs.social,
            data: [0, 0, 0, 63],
            backgroundColor: "#795548"
        }
    ]
});