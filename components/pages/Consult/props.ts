import locales from '../../../core/locales';
import { Post } from '../types/Post';

export type Props = {
  language: typeof locales;
  posts: Post[];
}
