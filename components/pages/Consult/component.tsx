import Head from 'next/head';

import {
  Anchor,
  AnchorButton,
  Button,
  Card,
  Container, Grid, GridItem, Heading,
  MainTemplate, PostCard, PostCardAuthor, PostCardBody, PostCardCover, PostCardPublished,
  SectionHeader, TitleHeader,
} from '../../index';
import { GetServerSideProps } from 'next';
import { Props } from './props';
import React, { FC, useState } from 'react';
import * as mock from '../../templates/MainTemplate/mock';
import locales from '../../../core/locales';
import { Paragraph } from '../../atoms/Paragraph';
import { useRouter } from 'next/router';
import { apiBaseUrl, controller } from '../../../core/api';
import { useTheme } from '@emotion/react';
import moment from 'moment';
import Crypto from 'crypto-js';
import { ResponseMessage } from '../../molecules/ResponseMessage';

const formatDate = (date) => moment(String(date)).format("DD.MM.YYYY hh:mm");

const Consult: FC<Props> = ({ posts, language }: Props) => {
  const theme = useTheme();
  const router = useRouter();

  const [name, setName] = useState("");
  const [contact, setContact] = useState("");
  const [message, setMessage] = useState("");

  const handleNameChange = (event) => setName(event.target.value);
  const handleContactChange = (event) => setContact(event.target.value);
  const handleMessageChange = (event) => setMessage(event.target.value);

  const [response, setResponse] = useState('');

  const submitForm = () => {
     controller.get(`/feedback/add?token=${Crypto.MD5(`free${moment().format("DDMM")}admin`).toString()}&name=${name}&phone=${contact}&message=${message}`)
      .then(response => {
        response.data.message === "success" ? setResponse("Ваша заявка принята!") : setResponse("Произошла какая то ошибка! Попробуйте позже!");
      })
      .catch(err => {
        setResponse("Произошла какая то ошибка! Попробуйте позже!");
      });
  }

  return (
    <div>
      <MainTemplate
        header={mock.header}
        footer={mock.footer}
        isFixed
      >
        <Head>
          <title>{language.consult.title}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          <SectionHeader cover={"/consult.png"} css={{ padding: "200px 0" }}>
            <Heading className="mb-3" as="h1" css={{ color: "#fff" }}>{language.consult.title}</Heading>
            <Paragraph css={{ maxWidth: 550, margin: "auto", marginTop: 0, color: "#fff" }}>{language.consult.description}</Paragraph>
            <Button className="d-inline-block mt-4 py-2" onClick={() => router.push(router.asPath + "#consult")}>{language.consult.write}</Button>
          </SectionHeader>
          <Container>
            <div className="d-flex align-items-center py-5">
              <div className="col-8 pr-5 mb-5">
                <Heading as="h3" css={{ fontSize: 30, marginBottom: "10px" }}>{language.consult.defense_title}</Heading>
                <Paragraph>
                  {language.consult.defense_paragraph}
                </Paragraph>
                <div className="lawyer d-flex align-items-center p-3 px-4 mt-4" css={{ background: theme.darkBg, borderRadius: 15 }}>
                  <img css={{ width: 70 }} src="/elzhan.png" alt=""/>
                  <div className="col pr-0" css={{ color: theme.textColor }}>
                    <p css={{ fontSize: 18, fontStyle: "italic", marginBottom: 5, marginTop: 0 }}>{language.consult.quote}</p>
                    <span css={{ opacity: .7, fontSize: 13 }}>{language.consult.lawyer_info}</span>
                  </div>
                </div>
              </div>
              <div className="col-4 p-0">
                <img className="d-block w-100" src="/consult-about.png" alt=""/>
              </div>
            </div>
          </Container>
          <div className="py-5" css={{ backgroundColor: theme.darkBg }}>
            <Container>
              <TitleHeader
                css={{ marginBottom: 20, background: theme.globalBg }}
                subtitle={
                  <Button className="py-2" css={{ fontSize: 12 }} onClick={() => router.push('/cases')}>{language.cases.all}</Button>
                }
              >
                <Heading as="span" css={{ display: "block", fontSize: 20, fontWeight: 700 }}>{language.cases.title}</Heading>
              </TitleHeader>
              <Grid columns={3} gap="20px">
                {posts.map((n, i) => (
                  <GridItem columns={1} key={i}>
                    <PostCard className="h-100">
                      <PostCardCover url={apiBaseUrl + n.cover.url}/>
                      <PostCardBody css={{ background: theme.globalBg }}>
                        <Heading className="mt-0" as="span" css={
                          {
                            textTransform: "uppercase",
                            display: "block",
                            fontSize: 17,
                            fontWeight: 700,
                            lineHeight: 1.6
                          }
                        }>
                          <AnchorButton
                            className="d-block"
                            css={{
                              color: 'inherit',
                              fontWeight: "inherit",
                              fontSize: "inherit"
                            }}
                            onClick={() => router.push(`/post/${n.id}`)}
                          >
                            {n.title}
                          </AnchorButton>
                        </Heading>
                        <div className="d-flex mt-3">
                          <PostCardAuthor href={n.author_link}
                                          css={{marginRight: 10, fontWeight: 700}}>
                            {n.author}
                          </PostCardAuthor>
                          <PostCardPublished>{formatDate(n.published)}</PostCardPublished>
                        </div>
                      </PostCardBody>
                    </PostCard>
                  </GridItem>
                ))}
              </Grid>

            </Container>
          </div>
          <div id="consult" className="py-5">
            <Container>
            <TitleHeader
                css={{ marginBottom: 10, background: theme.darkBg }}
                subtitle={
                  <Anchor className="d-flex align-items-center" href="https://t.me/info_ifkz" css={{ width: 300, background: "#106acc", borderRadius: 100, padding: 5, textAlign: "center", transition: "background 0.1s", "&:hover": { background: "#2196F3" } }}>
                    <img src="/telegram.png" alt="Telegram IFKZ" css={{ width: 25, marginRight: 10, filter: "invert(1)" }} />
                    <span className="col" css={{ fontWeight: 600, textTransform: "uppercase", color: "#fff", fontSize: 13 }}>{language.consult.telegram_write}</span>
                  </Anchor>
                }
              >
                <Heading as="span" css={{ display: "block", fontSize: 20, fontWeight: 700 }}>{language.consult.feedback_title}</Heading>
              </TitleHeader>
              {response && <ResponseMessage>{response}</ResponseMessage>}
              <Card>
                <div className="d-flex">
                  <input type="text" className="d-block col p-3 mr-1" value={name} onChange={handleNameChange} placeholder={language.consult.feedback_name} css={{ fontFamily: "inherit", fontSize: 14, border: 0, fontWeight: 600, borderRadius: 7, color: theme.textColor, background: theme.lightBg }} />
                  <input type="text" className="d-block col p-3 ml-1" value={contact} onChange={handleContactChange} placeholder={language.consult.feedback_phone} css={{ fontFamily: "inherit", fontSize: 14, border: 0, fontWeight: 600, borderRadius: 7, color: theme.textColor, background: theme.lightBg }} />
                </div>
                <textarea className="d-block py-2 px-3 mt-2" value={message} onChange={handleMessageChange} placeholder={language.consult.feedback_message} css={{ fontFamily: "inherit", fontSize: 14, border: 0, fontWeight: 600, borderRadius: 7, width: "100%", maxWidth: "100%", minWidth: "100%", minHeight: 200, maxHeight: 500, color: theme.textColor, background: theme.lightBg }} />
                <div className="text-center">
                  <Button className="mt-3 py-2" css={{ fontSize: 12 }} onClick={submitForm}>{language.consult.feedback_button}</Button>
                </div>
              </Card>
            </Container>
          </div>
        </main>
      </MainTemplate>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
  const language = ctx.locale || ctx.defaultLocale;
  const res = await controller.get(`/cases/last`);
  const posts = await res.data.body;
  const meta = await res.data.meta;
  return { props: { posts, language: locales[language] } }
}

export default Consult;
