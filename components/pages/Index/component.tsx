import Head from 'next/head';

import {
  Anchor,
  AnchorButton,
  Button,
  Card,
  Container, Grid, GridItem, Heading,
  MainTemplate, PostCard, PostCardAuthor, PostCardBody, PostCardCover, PostCardPublished,
  WorkCard,
  SectionHeader, TitleHeader, RegistryModule,
} from '../../index';
import { GetServerSideProps } from 'next';
import { Props } from './props';
import React, { FC, useEffect, useState } from 'react';
import * as mock from '../../templates/MainTemplate/mock';
import locales from '../../../core/locales';
import { Paragraph } from '../../atoms/Paragraph';
import { useRouter } from 'next/router';
import { apiBaseUrl, controller } from '../../../core/api';
import { useTheme } from '@emotion/react';
import moment from 'moment';
import { CheckModule } from '../../organisms/CheckModule';


const formatDate = (date) => moment(String(date)).format("DD.MM.YYYY hh:mm");

const Index: FC<Props> = ({ posts, language, checkNodes }: Props) => {
  const theme = useTheme();
  const router = useRouter();

  const works = [
    {
      img: "check.svg",
      description: language.home.tab_first,
      url: "#app"
    },
    {
      img: "registry.svg",
      description: language.home.tab_second,
      url: "/registry"
    },
    {
      img: "consult.svg",
      description: language.home.tab_fourth,
      url: "/consult"
    },
    {
      img: "news.svg",
      description: language.home.tab_third,
      url: "/news"
    },
  ];

  return (
    <div>
      <MainTemplate
        header={mock.header}
        footer={mock.footer}
        isFixed
      >
        <Head>
          <title>{language.home.title}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          <CheckModule language={language} checkNodes={checkNodes} />
          <div className="py-5" css={{ backgroundColor: theme.darkBg }}>
            <Container>
              <TitleHeader
                css={{ marginBottom: 20, background: theme.globalBg }}
                subtitle={
                  <Button className="py-2" css={{ fontSize: 12 }} onClick={() => router.push('/blog')}>{language.news.all}</Button>
                }
              >
                <Heading as="span" css={{ display: "block", fontSize: 20, fontWeight: 700 }}>{language.news.title}</Heading>
              </TitleHeader>
              <Grid columns={3} gap="20px">
                {posts.map((n, i) => (
                  <GridItem columns={1} key={i}>
                    <PostCard className="h-100">
                      <PostCardCover url={apiBaseUrl + n.cover.url}/>
                      <PostCardBody css={{ background: theme.globalBg }}>
                        <Heading className="mt-0" as="span" css={
                          {
                            textTransform: "uppercase",
                            display: "block",
                            fontSize: 17,
                            fontWeight: 700,
                            lineHeight: 1.6
                          }
                        }>
                          <AnchorButton
                            className="d-block"
                            css={{
                              color: 'inherit',
                              fontWeight: "inherit",
                              fontSize: "inherit"
                            }}
                            onClick={() => router.push(`/post/${n.id}`)}
                          >
                            {n.title}
                          </AnchorButton>
                        </Heading>
                        <div className="d-flex mt-3">
                          <PostCardAuthor href={n.author_link}
                                          css={{marginRight: 10, fontWeight: 700}}>
                            {n.author}
                          </PostCardAuthor>
                          <PostCardPublished>{formatDate(n.published)}</PostCardPublished>
                        </div>
                      </PostCardBody>
                    </PostCard>
                  </GridItem>
                ))}
              </Grid>

              <Card className="mt-4" css={{ background: theme.globalBg }}>
                <Heading className="text-center" as="h1" css={{ display: "block", fontSize: 25, fontWeight: 700, marginBottom: 10, marginTop: 10 }}>{language.home.title}</Heading>
                <iframe className="mt-3 mb-4" css={{ borderRadius: 10, border: "none" }} width="100%" height="400px" src="https://www.youtube.com/embed/3H4C3aFEkIs" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" />

                <Paragraph css={{ padding: 0, margin: 0, fontSize: 17, lineHeight: 1.5 }}>
                  {language.home.description_first}
                  <br/>
                  <b>{language.home.description_second}</b>
                  <br/>
                  {language.home.description_third}
                </Paragraph>
                <ul css={{ padding: 0, paddingLeft: 20, lineHeight: 1.5 }}>
                  <li>{language.home.list_first}</li>
                  <li>{language.home.list_second}</li>
                  <li>{language.home.list_third}</li>
                  <li>{language.home.list_fourth}</li>
                </ul>
              </Card>

              <TitleHeader
                className="mt-5"
                css={{ marginBottom: 20, background: theme.globalBg }}
              >
                <Heading as="span" css={{ display: "block", fontSize: 20, fontWeight: 700 }}>{language.home.our_work}</Heading>
              </TitleHeader>
              <Grid columns={4} gap="10px">
                {works.map((n, i) => (
                  <WorkCard title={n.description} url={n.url} cover={`/images/${n.img}`} buttonLabel={language.common.more} key={i} />
                ))}
              </Grid>
            </Container>
          </div>
        </main>
      </MainTemplate>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
  const language = ctx.locale || ctx.defaultLocale;
  const res = await controller.get(`/news/all?page=1&lang=${language}`);
  const posts = await res.data.body.slice(0, 3);
  const checkNodesRes = await controller.get('/nodes/all')
  return { props: { posts, language: locales[language], checkNodes: checkNodesRes.data.body } }
}

export default Index;
