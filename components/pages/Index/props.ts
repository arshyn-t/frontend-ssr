import locales from '../../../core/locales';
import { Post } from '../types/Post';
import { CheckNode } from '../../organisms/CheckModule';

export type Props = {
  language: typeof locales;
  posts: Post[];
  checkNodes: CheckNode[];
}
