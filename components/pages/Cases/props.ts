import { Post } from "../types/Post";
import { PostMeta } from '../types/PostMeta';
import locales from '../../../core/locales';

export type Props = {
  posts: Post[],
  postsMeta: PostMeta,
  language: typeof locales
}
