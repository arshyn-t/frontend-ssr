import Head from 'next/head';
import { apiBaseUrl, controller } from '../../../core/api';

import {
  Heading,
  AnchorButton,
  PostCard,
  PostCardLarge,
  PostCardAuthor,
  PostCardBody,
  PostCardCover,
  PostCardPublished,
  Container,
  Grid,
  GridItem, TitleHeader, MainTemplate, Pagination,
} from '../../index';
import moment from 'moment';
import { css } from '@emotion/react';
import { GetServerSideProps } from 'next';
import { Props } from './props';
import React, { FC } from 'react';
import * as mock from '../../templates/MainTemplate/mock';
import locales from '../../../core/locales';
import { useRouter } from 'next/router';

const formatDate = (date) => moment(String(date)).format("DD.MM.YYYY hh:mm");
const columnPerRow = (id: number, postsLength: number): number => {
  if((postsLength%8 - (id%8+1) == postsLength%8-2) || (postsLength%8 - (id%8+1) == postsLength%8-3) || (postsLength%8 - (id%8+1) == postsLength%8-4)){
    return 4;
  }
  else if((postsLength%8 - (id%8+1) == postsLength%8-5) || (postsLength%8 - (id%8+1) == postsLength%8-6) || (postsLength%8 - (id%8+1) == postsLength%8-7) || (postsLength%8 - (id%8+1) == postsLength%8-8)){
    return 3;
  }
  return 12;
}

const gridHover = css`
  &:hover {
   ${PostCard}, ${PostCardLarge} {
    opacity: .5;
   }
  }
  ${PostCard}:hover, ${PostCardLarge}:hover {
    opacity: 1;
  }
`;

const Cases: FC<Props> = ({ posts, postsMeta, language }: Props) => {
  const router = useRouter();

  const handlePaginationClick = (number) => () => {
    router.push(`${router.pathname}?page=${number}`);
  }

  return (
    <div>
      <MainTemplate
        header={mock.header}
        footer={mock.footer}
      >
        <Head>
          <title>{language.cases.title}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          <Container>
            <TitleHeader className="mb-4" subtitle={`${postsMeta.count} ${language.cases.publications}`}>
              <Heading className="m-0" as="h3">
                {language.cases.title}
              </Heading>
            </TitleHeader>
            <Grid className="mb-4" columns={12} gap="20px" css={gridHover}>
              {posts && posts.map((n, i) =>
                <GridItem columns={columnPerRow(i, postsMeta.count)} key={i}>
                  {columnPerRow(i, postsMeta.count) === 12 ?
                    <PostCardLarge cover={apiBaseUrl + n.cover.url}>
                      <PostCardBody className="pb-4">
                        <Heading className="mt-0" as="span" css={
                          {
                            textTransform: "uppercase",
                            display: "block",
                            fontSize: 30,
                            fontWeight: 700,
                            lineHeight: 1.6
                          }
                        }>
                          <AnchorButton
                            className="d-block"
                            css={{
                              color: 'inherit',
                              fontWeight: "inherit",
                              fontSize: "inherit"
                            }}
                            onClick={() => router.push(`/post/${n.id}`)}
                          >
                            {n.title}
                          </AnchorButton>
                        </Heading>
                        <div className="d-flex mt-2">
                          <PostCardAuthor href={n.author_link}
                                          css={{marginRight: 10, fontWeight: 700 }}>
                            {n.author}
                          </PostCardAuthor>
                          <PostCardPublished>{formatDate(n.published)}</PostCardPublished>
                        </div>
                      </PostCardBody>
                    </PostCardLarge>
                    :
                    <PostCard className="h-100">
                      <PostCardCover url={apiBaseUrl + n.cover.url}/>
                      <PostCardBody>
                        <Heading className="mt-0" as="span" css={
                          {
                            textTransform: "uppercase",
                            display: "block",
                            fontSize: 17,
                            fontWeight: 700,
                            lineHeight: 1.6
                          }
                        }>
                          <AnchorButton
                            className="d-block"
                            css={{
                              color: 'inherit',
                              fontWeight: "inherit",
                              fontSize: "inherit"
                            }}
                            onClick={() => router.push(`/post/${n.id}`)}
                          >
                            {n.title}
                          </AnchorButton>
                        </Heading>
                        <div className="d-flex mt-3">
                          <PostCardAuthor href={n.author_link}
                                          css={{marginRight: 10, fontWeight: 700}}>
                            {n.author}
                          </PostCardAuthor>
                          <PostCardPublished>{formatDate(n.published)}</PostCardPublished>
                        </div>
                      </PostCardBody>
                    </PostCard>
                  }
                </GridItem>
              )}
            </Grid>
            <Pagination
              className="mb-5"
              basePath={router.pathname}
              perPage={postsMeta.per_page}
              count={postsMeta.count}
              onClick={handlePaginationClick}
              active={parseInt(typeof router.query.page === "string" ? router.query.page : "1")}
            />

          </Container>
        </main>
      </MainTemplate>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
  const language = ctx.locale || ctx.defaultLocale;
  const pageNumber = ctx.query.page || 1;
  const res = await controller.get(`/cases/all?page=${pageNumber}&lang=${language}`);
  const posts = await res.data.body;
  const meta = await res.data.meta;

  return { props: { posts, postsMeta: meta, language: locales[language] } }
}

export default Cases;
