import Head from 'next/head';

import {
  Container, Heading,
  MainTemplate,
  Grid, GridItem,
  Card, Button
} from '../../index';
import { GetServerSideProps } from 'next';
import { Props } from './props';
import React, { FC } from 'react';
import * as mock from '../../templates/MainTemplate/mock';
import locales from '../../../core/locales';
import { Paragraph } from '../../atoms/Paragraph';
import { useTheme } from '@emotion/react';
import { useRouter } from 'next/router';

const Partners: FC<Props> = ({ language }: Props) => {
  const theme = useTheme();
  const router = useRouter();
  
  return (
    <div>
      <MainTemplate
        header={mock.header}
        footer={mock.footer}
      >
        <Head>
          <title>{language.partners.title}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          <Container>
            <Heading as="h1">{language.partners.title}</Heading>
            <Grid columns={2} gap="30px">
              <GridItem columns={1}>
                <Card css={{ height: "100%", background: theme.darkBg, color: theme.textColor, textAlign: "center", padding: 30, borderRadius: 20, display: "flex", alignItems: "flex-start" }}>
                  <div>
                    <img src="/partners/soros.png" alt={language.partners.soros_title} css={{ maxWidth: 200, borderRadius: 10 }} />
                    <Button className="mt-2 py-2 d-block w-100" css={{ fontSize: 13 }} onClick={() => router.push("https://www.soros.kz/ru/")}>Перейти на сайт</Button>
                  </div>
                  <div className="text-left ml-4">
                    <Heading as="h3" className="m-0">{language.partners.soros_title}</Heading>
                    <Paragraph className="mt-1" css={{ fontSize: 13 }}>
                      {language.partners.soros_description}
                    </Paragraph>
                  </div>
                </Card>
              </GridItem>
              <GridItem columns={1}>
                <Card css={{ height: "100%", background: theme.darkBg, color: theme.textColor, textAlign: "center", padding: 30, borderRadius: 20, display: "flex", alignItems: "flex-start" }}>
                  <div>
                    <img src="/partners/mediacenter.png" alt={language.partners.soros_title} css={{ maxWidth: 200, borderRadius: 10 }} />
                    <Button className="mt-2 py-2 d-block w-100" css={{ fontSize: 13 }} onClick={() => router.push("https://lmc.kz/ru")}>Перейти на сайт</Button>
                  </div>
                  <div className="text-left ml-4">
                    <Heading as="h3" className="m-0">{language.partners.media_title}</Heading>
                    <Paragraph className="mt-1" css={{ fontSize: 13 }}>
                      {language.partners.media_description}
                    </Paragraph>
                  </div>
                </Card>
              </GridItem>
            </Grid>
          </Container>
        </main>
      </MainTemplate>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
  const language = ctx.locale || ctx.defaultLocale;
  return { props: { language: locales[language] } }
}

export default Partners;
