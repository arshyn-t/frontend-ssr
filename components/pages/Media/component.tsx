import Head from 'next/head';
import { apiBaseUrl, controller } from '../../../core/api';

import {
  Heading,
  AnchorButton,
  PostCard,
  PostCardLarge,
  PostCardBody,
  Container,
  Grid,
  GridItem, TitleHeader, MainTemplate, Pagination,
} from '../../index';
import moment from 'moment';
import { css } from '@emotion/react';
import { GetServerSideProps } from 'next';
import { Props } from './props';
import React, { FC } from 'react';
import * as mock from '../../templates/MainTemplate/mock';
import locales from '../../../core/locales';
import { useRouter } from 'next/router';

const formatDate = (date) => moment(String(date)).format("DD.MM.YYYY hh:mm");

const gridHover = css`
  &:hover {
   ${PostCard}, ${PostCardLarge} {
    opacity: .5;
   }
  }
  ${PostCard}:hover, ${PostCardLarge}:hover {
    opacity: 1;
  }
`;

const Media: FC<Props> = ({ posts, postsMeta, language }: Props) => {
  const router = useRouter();


  return (
    <div>
      <MainTemplate
        header={mock.header}
        footer={mock.footer}
      >
        <Head>
          <title>{language.smi.title}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          <Container>
            <TitleHeader className="mb-4" subtitle={`${postsMeta.count} ${language.cases.publications}`}>
              <Heading className="m-0" as="h3">
                {language.smi.title}
              </Heading>
            </TitleHeader>
            <Grid className="mb-4" columns={3} gap="20px" css={gridHover}>
              {posts && posts.map((n, i) =>
                <GridItem columns={1} key={i}>
                  <PostCard className="h-100">
                    <div className="p-2" css={{ borderRadius: 10, overflow: "hidden" }}>
                      <img src={apiBaseUrl + n.image.url} alt={n.title} css={{ background: "#fff", width: "100%", display: "block", borderRadius: 10 }} />
                    </div>
                    <PostCardBody>
                      <Heading className="mt-0" as="span" css={
                        {
                          textTransform: "uppercase",
                          display: "block",
                          fontSize: 17,
                          fontWeight: 700,
                          lineHeight: 1.6
                        }
                      }>
                        <AnchorButton
                          className="d-block"
                          css={{
                            color: 'inherit',
                            fontWeight: "inherit",
                            fontSize: "inherit"
                          }}
                          onClick={() => router.push(`/post/${n.id}`)}
                        >
                          {n.title}
                        </AnchorButton>
                      </Heading>
                    </PostCardBody>
                  </PostCard>
                </GridItem>
              )}
            </Grid>
          </Container>
        </main>
      </MainTemplate>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
  const language = ctx.locale || ctx.defaultLocale;
  const res = await controller.get(`/smi/all`);
  const posts = await res.data.body;
  const meta = await res.data.meta;

  return { props: { posts, postsMeta: meta, language: locales[language] } }
}

export default Media;
