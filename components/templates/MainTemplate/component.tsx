import styled from '@emotion/styled';
import React, { FC, useContext, useEffect, useState } from 'react';
import { Transition } from 'react-transition-group';
import classNames from 'classnames';

import { Anchor } from '../../atoms/Anchor';
import { AnchorButton } from '../../atoms/AnchorButton';
import { Butter } from '../../molecules/Butter';
import { LocaleSelect } from '../../molecules/LocaleSelect';
import { Navigation, NavigationItem } from '../../molecules/Navigation';
import { ThemeSwitch } from '../../molecules/ThemeSwitch';
import { Footer } from '../../organisms/Footer';
import { FooterAbout } from '../../organisms/FooterAbout';
import { FooterWrapper } from '../../organisms/FooterWrapper';
import { Header } from '../../organisms/Header';
import { Logo } from '../../organisms/Header/libs/Logo';
import { Props } from './props';
import { useAction, useAtom } from '@reatom/react';
import { changeTheme, themeAtom } from '../../../store/themeAtom';

import locales from '../../../core/locales';
import { useRouter } from 'next/router';
import { useTheme } from '@emotion/react';
import { colors } from '../../../core';

const MainBody = styled.div`
  flex: 1;
`;

const MainTemplateBase: FC<Props> = ({
  footer,
  isFixed,
  header,
  children,
  ...rest
}: Props) => {
  const [isNavActive, setIsNavActive] = useState(false);
  const { locale, push: routerPush } = useRouter();
  const language = locales[locale];
  const [scrolled, setScrolled] = useState(false);

  const toggleNav = (): void => {
    setIsNavActive(!isNavActive);
  }

  const theme = useAtom(themeAtom);
  const setTheme = useAction((payload) => changeTheme(payload));
  const themeColors = useTheme();

  const handleScroll = (event) => {
    if (window.scrollY < 100) {
      setScrolled(false);
    }
    else if (window.scrollY >=  100) {
      setScrolled(true);
    }
  }

  const switchTheme = () => {
    const newTheme = theme === "dark" ? "light" : "dark";
    setTheme(newTheme);
    localStorage.setItem("theme", newTheme);
  }

  useEffect(() => {
    setTheme(localStorage.theme || "dark");
    window.addEventListener('scroll', handleScroll);
  }, [setTheme]);

  return (
    <div {...rest}>
      <Header className={classNames({ "position-fixed": isFixed })} css={{ background: (isFixed && scrolled ? colors.darkBg : "transparent")}}>
        <AnchorButton onClick={() => routerPush('/')}>
          <Logo src={header.logo.img} alt="Internet Freedom">
            <span css={{ color: isFixed ? "#fff" : "inherit" }}>INTERNET FREEDOM</span>
            <br />
            <b>Kazakhstan</b>
          </Logo>
        </AnchorButton>
        <Butter onClick={toggleNav} {...(isFixed ? { css: { color: "#fff" } }: {})}>
          {language.navigation.title}
        </Butter>
        {(
          <Transition
            in={isNavActive}
            timeout={{ enter: 0, exit: 200 }}
            mountOnEnter
            unmountOnExit
          >
            {state => (
              <div css={{ position: "fixed", zIndex: -3, top: 0, right: 0 }}>
                <button  className={classNames("shade", state)} onClick={toggleNav} />
                <Navigation className={state} >
                  {header.links.map((item, i) => (
                    <NavigationItem
                      onClick={() => routerPush(item.link)}
                      className="navigation__link"
                      key={i}
                    >
                      {language.navigation[item.text]}
                    </NavigationItem>
                  ))}
                  <ThemeSwitch isActive={theme === "light"}  onClick={switchTheme} className="mt-4">
                    Тема
                  </ThemeSwitch>
                  <LocaleSelect
                    className="mt-3"
                    languages={header.languages}
                    locale={language.id}
                  />
                </Navigation>
              </div>
            )}
          </Transition>
        )}
      </Header>
      <MainBody>{children}</MainBody>
      <FooterWrapper>
        <FooterAbout title={language.socials.title}>{language.socials.description}</FooterAbout>
        <Footer>
          <div className="socials__partners--wrapper">
            <AnchorButton className="social__links" onClick={() => routerPush('partners')} css={{ "&:hover": { color: themeColors.accentBlue, transform: "none!important" } }}>
              {language.socials.partners}
            </AnchorButton>
            <Anchor className="social__links" href='/privacy_policy.docx'>
              {language.socials.privacy_policy}
            </Anchor>
          </div>
          <div className="socials__info">
            <h3 className="title">{language.socials.follow}</h3>
            <div className="socials-grid">
              {footer.socials.map((social, i) => (
                <div className="social" key={i}>
                  <Anchor href={social.url}>
                    <img src={social.img} alt="" className="social__img" />
                  </Anchor>
                </div>
              ))}
            </div>
          </div>
        </Footer>
      </FooterWrapper>
    </div>
  );
};

export const MainTemplate = styled(MainTemplateBase)<Pick<Props, 'theme'>>`
  display: flex;
  min-height: 100vh;
  flex-direction: column;
  background: ${({ theme }) => theme.globalBg };
`;
