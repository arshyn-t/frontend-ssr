import { HTMLAttributes } from 'react';

import { ColorProps } from '../../../core/colors';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly footer: {
    readonly title: string;
    readonly socials: ReadonlyArray<Record<string, string>>;
  };
  readonly header: {
    readonly logo: Record<string, string>;
    readonly theme: string;
    readonly links: ReadonlyArray<Record<string, string>>;
    readonly languages: Record<string, string>;
  };
  readonly theme?: ColorProps;
  readonly isFixed?: boolean;
};
