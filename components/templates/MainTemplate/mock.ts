export const header = {
  logo: {
    url: '/',
    img: 'https://ifkz.org/src/assets/logo.png',
  },
  theme: 'Theme',
  links: [
    { link: '/registry', text: 'registry' },
    { link: '/stats', text: 'stats' },
    { link: '/blog', text: 'blog' },
    { link: '/cases', text: 'cases' },
    { link: '/media', text: 'media' },
    { link: '/consult', text: 'consult' },
    { link: '/contacts', text: 'contacts' },
  ],
  languages: {
    ru: 'https://ifkz.org/src/assets/locales/ru.png',
    en: 'https://ifkz.org/src/assets/locales/en.png',
    kk: 'https://ifkz.org/src/assets/locales/kk.png',
  },
};

export const footer = {
  title: 'Our social medias',
  socials: [
    {
      img: 'https://ifkz.org/src/assets/popular/instagram.png',
      url: 'https://instagram.com/internetfreedom_kz?igshid=g7nrax74t8te',
    },
    {
      img: 'https://ifkz.org/src/assets/popular/facebook.png',
      url: 'https://www.facebook.com/internetfreedomkz/',
    },
    {
      img: 'https://ifkz.org/src/assets/popular/telegram.png',
      url: 'https://t.me/InternetFreedomKZ',
    },
  ],
};
