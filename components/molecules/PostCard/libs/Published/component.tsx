import styled from '@emotion/styled';
import React, { FC } from 'react';

import { Props } from './props';

const PublishedBase: FC<Props> = ({ children, ...rest }: Props) => (
  <span {...rest}>{children}</span>
);

export const Published = styled(PublishedBase)<Pick<Props, 'theme'>>`
  display: inline-block;
  font-size: 11px;
  font-weight: 400;
  text-transform: lowercase;
  opacity: 0.7;
  color: ${({ theme }) => theme.textColor};
`;
