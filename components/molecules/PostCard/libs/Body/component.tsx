import styled from '@emotion/styled';

export const Body = styled.div`
  flex-grow: 1;
  padding: 20px 25px 60px;
`;
