import { Props } from '../props';

export type ResponseMessageString = {
  text?: string;
} & Pick<Props, 'type'>;
