import styled from '@emotion/styled';
import { Props } from './props';
import React, { FC } from 'react';

const PostCoverBase: FC<Props> = ({ src, alt, ...rest }: Props) => (
  <div {...rest}>
    <img src={src} alt={alt} />
    <div className="blur" />
  </div>
);


export const PostCover = styled(PostCoverBase)<Props>`
  background-image: linear-gradient(to top, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), ${({ src }) => `url(${src})`};
  background-position: center;
  background-size: cover;
  height: 500px;
  position: relative;
  border-radius: 10px;
  overflow: hidden;

  img {
    position: relative;
    z-index: 1;
    margin: auto;
    border-radius: 10px;
    display: block;
    height: 100%;
  }
  .blur {
    backdrop-filter: blur(50px);
    position: absolute;
    top: 0;
    width: 100%;
    height: 100%;
  }
`
