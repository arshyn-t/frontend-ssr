import { ReactNode } from 'react';

import { CardProps } from '../Card';

export type Props = CardProps & {
  readonly subtitle?: ReactNode | string | number;
};
