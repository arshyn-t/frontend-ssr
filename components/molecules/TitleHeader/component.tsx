import styled from '@emotion/styled';
import classNames from 'classnames';
import React, { FC } from 'react';

import { Card } from '../Card';
import { Props } from './props';

const Subtitle = styled.span<Pick<Props, 'theme'>>`
  color: ${({ theme }) => theme.textColor};
  font-size: 13px;
  opacity: 0.6;
  font-weight: 400;
`;

export const TitleHeader: FC<Props> = ({ subtitle, children, ...rest }: Props) => (
  <Card {...rest}>
    <div className={classNames('d-flex', 'justify-content-between', 'align-items-center')}>
      {children}
      {subtitle && ['string', 'number'].includes(typeof subtitle) ? (
        <Subtitle>{subtitle}</Subtitle>
      ) : (
        subtitle
      )}
    </div>
  </Card>
);
