import { CardProps } from "../Card";

export type Props = CardProps & {
    readonly title: string;
    readonly buttonLabel: string;
    readonly cover: string;
    readonly url: string;
}