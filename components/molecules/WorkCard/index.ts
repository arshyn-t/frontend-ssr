import { Props } from './props';

export * from './component';

export type WorkCardProps = Props;