import React, { FC } from "react";
import { useRouter } from "next/router";
import { Props } from './props';
import { Card } from "../Card";
import { AnchorButton } from "../../atoms/AnchorButton";
import { Heading } from '../../atoms/Heading';
import { useTheme, css } from "@emotion/react";
import styled from "@emotion/styled";
import { Button } from "../../atoms/Button";

const WorkCardBase: FC<Props> = ({ title, cover, url, buttonLabel, ...rest }: Props) => {
    const router = useRouter();
    const onClick = () => router.push(url);
    const theme = useTheme();

    return (
        <Card {...rest}>
            <AnchorButton onClick={onClick}>
                <img className="d-block w-100" css={{ borderRadius: 30, boxShadow: theme.blockShadow }} src={cover} alt={title} />
            </AnchorButton>
            <div className="d-flex flex-column col p-0">
                <Heading as="span" className="d-block col mt-3 p-0" css={{ fontSize: 20, fontWeight: 600 }}>{title}</Heading>
                <Button className="py-2 mt-4" css={{ height: "auto", fontSize: 12 }} onClick={onClick}>{buttonLabel}</Button>
            </div>
        </Card>
    );
}

export const WorkCard = styled(WorkCardBase)<Props>`
    display: flex;
    flex-direction: column;
    border-radius: 20px;
    padding: 20px;
    transition: background 0.2s, box-shadow 0.2s;
    &:hover{
        ${({ theme }) => css`
            background: ${theme.globalBg};
            box-shadow: ${theme.blockShadow};
        `}
    }    
`;