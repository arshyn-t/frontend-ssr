import classNames from 'classnames';
import React, { FC } from 'react';

import { Card } from '../Card';
import { Props } from './props';
import styled from '@emotion/styled';

const PostCardLargeBase: FC<Props> = ({ className, children, ...rest }: Props) => {
  return (
    <Card
      className={classNames(className, 'd-flex', 'flex-column', 'px-0')}
      {...rest}
    >
      {children}
    </Card>
  );
};

export const PostCardLarge = styled(PostCardLargeBase)<Props>`
  background-image: ${({ theme }) => theme.theme === "light" ? `linear-gradient(to top, #efefef 32%, transparent 0%),linear-gradient(to top, rgb(173 173 173 / 75%), rgb(197 197 197 / 75%))` : `linear-gradient(to top, rgba(33, 45, 113, 0.73) 23%, transparent 44%), linear-gradient(to top, rgba(63, 81, 181, 0.75), rgba(33, 150, 243, 0.75))`}, ${({ cover }) => `url(${cover})`};
  background-size: cover;
  padding-top: 25%;
  background-position: center;
  transition: transform 0.2s, opacity 0.2s;
  &:hover {
    transform: scale(1.02);
  }
`;
