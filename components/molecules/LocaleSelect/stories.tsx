import React, { FC } from 'react';

import { Container } from '../../atoms/Container';
import { MockBackground } from '../../atoms/MockBackground';
import { LocaleSelect } from './component';

export default {
  title: 'Organisms/Header',
};

export const LocaleSelectBasic: FC = () => {
  const languages = {
    ru: 'https://ifkz.org/src/assets/locales/ru.png',
    en: 'https://ifkz.org/src/assets/locales/en.png',
    kk: 'https://ifkz.org/src/assets/locales/kk.png',
  };
  return (
    <>
      <Container mini>
        <MockBackground>
          <LocaleSelect languages={languages} locale="ru" />
        </MockBackground>
      </Container>
    </>
  );
};
