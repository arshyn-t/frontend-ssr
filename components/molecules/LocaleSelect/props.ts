import { HTMLAttributes, InputHTMLAttributes } from 'react';

export type Props = Omit<HTMLAttributes<HTMLDivElement>, 'onChange'> &
  Pick<InputHTMLAttributes<HTMLInputElement>, 'onChange'> & {
    readonly languages: Record<string, string>;
    readonly locale: string;
  };
