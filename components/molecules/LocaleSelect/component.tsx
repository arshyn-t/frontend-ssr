import styled from '@emotion/styled';
import React, { FC } from 'react';
import classNames from "classnames";

import { Props } from './props';
import { AnchorButton } from '../../atoms/AnchorButton';
import { useRouter } from 'next/router';

const LocaleSelectBase: FC<Props> = ({ languages, locale, onChange, ...rest }: Props) => {
  const router = useRouter();

  const switchLanguage = (locale) => () => {
    router.push(router.asPath, router.asPath, { locale })
  }

  return (
    <div {...rest}>
      <div className="switcher--container">
        <div className="switcher">
          {Object.keys(languages).map((language, i) => (
            <AnchorButton className={classNames("switcher__item", "w-100", { "active": locale === language })} key={i} onClick={switchLanguage(language)}>
              <img alt="" className="switcher__item--img" src={languages[language]} />
              {language}
            </AnchorButton>
          ))}
        </div>
      </div>
    </div>
  );
};

export const LocaleSelect = styled(LocaleSelectBase)`
  .switcher {
    margin-top: 0;
    display: block;
    background: rgba(255, 255, 255, 0.04);
    padding: 5px;
    border-radius: 22px;
    margin-bottom: 0;
  }
  .switcher__item {
    cursor: pointer;
    color: #fff !important;
    display: flex;
    align-items: center;
    border-radius: 100px;
    padding: 10px 15px;
    font-weight: 600;
    text-transform: uppercase;
    font-size: 12px;
    &.active {
      color: #fff;
      background: rgba(255, 255, 255, 0.2);
    }
  }
  .switcher__item--img {
    height: 11px;
    width: 15px;
    margin-right: 5px;
    display: block;
  }
`;
