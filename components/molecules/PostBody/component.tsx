import styled from '@emotion/styled';
import { Props } from './props';
import { css } from '@emotion/react';

export const PostBody = styled.div<Props>`
  ${
    ({ theme }) => css`
      color: ${theme.textColor};
      font-size: 16px;
    `
  }
`
