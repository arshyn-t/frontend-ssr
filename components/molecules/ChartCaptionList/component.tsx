import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { FC } from "react";
import { Props } from "./props";

const ChartCaptionListBase: FC<Props> = ({ data, ...rest }: Props) => (
    <ul {...rest}>
        {data.map((n, i) => (
            <li key={i} className="d-flex align-items-center py-3 col">
                <div css={{ background: n.color, borderRadius: 4, height: 15, width: 15, marginRight: 20, display: "inline-block" }} />
                <span css={{ fontSize: 14, lineHeight: 1.5 }}>
                    {n.caption}
                    <b> {n.subtitle}</b>
                </span>
            </li>
        ))}
    </ul>
);

export const ChartCaptionList = styled(ChartCaptionListBase)<Props>`
    list-style: none;
    text-align: left;
    display: flex;
    justify-content: space-around;
    ${
        ({ theme }) => css`
            background: ${theme.darkBg};
            color: ${theme.textColor};
        `
    }
    border-radius: 10px;
    padding: 20px;
    margin-bottom: 50px;
`;
