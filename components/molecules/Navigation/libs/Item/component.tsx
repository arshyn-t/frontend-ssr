import styled from '@emotion/styled';
import React, { FC } from 'react';

import { AnchorButton } from '../../../../atoms/AnchorButton';
import { Props } from './props';

const ItemBase: FC<Props> = ({ children, theme: _a, ...rest }: Props) => (
  <AnchorButton {...rest}>{children}</AnchorButton>
);

export const Item = styled(ItemBase)<Pick<Props, 'theme'>>`
  text-decoration: none;
  color: #fff;
  font-weight: 600;
  text-transform: uppercase;
  font-size: 20px;
  padding: 5px 0;
  transition: color 0.2s, transform 0.2s;
  &:hover {
    color: ${({ theme: colors }) => colors.accentBlue};
    transform: translateX(-10px);
  }
`;
