import { ColorProps } from '../../../../../core/colors';
import { AnchorButtonProps } from '../../../../atoms/AnchorButton';

export type Props = AnchorButtonProps & { readonly theme?: ColorProps };
