import { HTMLAttributes } from 'react';
import locales from '../../../core/locales';
import { CheckNode } from './types/CheckNode';

export type Props = HTMLAttributes<HTMLDivElement> & {
  language: typeof locales;
  checkNodes: CheckNode[];
};
