import React, { FC, FormEvent, useCallback, useEffect, useState } from 'react';
import { Props } from './props';
import { useTheme } from '@emotion/react';
import { Container } from '../../atoms/Container';
import { Heading } from '../../atoms/Heading';
import { Paragraph } from '../../atoms/Paragraph';
import { ResponseMessage, ResponseMessageString } from '../../molecules/ResponseMessage';
import { RegistryField } from '../RegistryModule';
import { Button } from '../../atoms/Button';
import { Card } from '../../molecules/Card';
import { controller } from '../../../core/api';
import { CheckResults } from './types/CheckResults';
import { CheckResult } from './libs/CheckResult';

export const CheckModule: FC<Props> = ({ language, checkNodes, ...rest }: Props) => {
  const theme = useTheme();
  const [searchQuery, setSearchQuery] = useState<string>();
  const [message, setMessage] = useState<ResponseMessageString>();
  const onFieldChange = (event: FormEvent<HTMLInputElement>) => {
    setSearchQuery(event.currentTarget.value);
  }
  const fieldDefaultStyles = { outline: "none" };
  var checkInterval;
  var checkCount = 1;

  const [checkResult, setCheckResult] = useState<CheckResults>(
    checkNodes.reduce((acc, n) => {
      return {
        ...acc,
        [n.asn_id]: {
          node: n
        }
      }
    }, {} as CheckResults)
  )

  const onClick = () => {
    checkCount = 1;

    resetResult()

    checkAvailability()
  }

  const resetResult = () => {

    setCheckResult(
        checkNodes.reduce((acc, n) => {
        return {
          ...acc,
          [n.asn_id]: {
            node: n
          }
        }
      }, {} as CheckResults)
    )
  }

  const watchAvailability = () => {
    checkCount++;
    checkAvailability();
  }

  const checkAvailability = () => {
    setMessage({ type: "loading"} );

    if(checkResult !== {}) {
      controller
        .get(`/website/check?url=${searchQuery}`)
        .then(res => {
          setMessage({type: "loading"})
          if (res.data.body.status === "pending" || res.data.body.status == "already checking") {
            if (checkInterval === undefined) {
              checkInterval = setInterval(watchAvailability, 5000)
            }
            if (checkCount === 3) {
              clearInterval(checkInterval)
              setMessage(null);

              const newCheckResult = checkResult;
              res.data.body.check_cache_responses.map(n => {
                newCheckResult[n.asn_id][n.scheme] = !!n.accessible
              })
              setCheckResult(newCheckResult)
              setMessage({type: "success", text: language.homecheck.success})
            }
          } else if (res.data.body.status === "ready") {
            if(res.data.body.check_cache_responses.length == checkNodes.length*2) {
              clearInterval(checkInterval)
              setMessage(null)

              const newCheckResult = checkResult;
              res.data.body.check_cache_responses.map(n => {
                newCheckResult[n.asn_id][n.scheme] = !!n.accessible
              })
              setCheckResult(newCheckResult)

              setMessage({type: "success", text: language.homecheck.success})
            }else {
              if (checkInterval === undefined) {
                checkInterval = setInterval(watchAvailability, 5000)
              }
            }
            if (checkCount === 3) {
              clearInterval(checkInterval)
              setMessage(null);

              const newCheckResult = checkResult;
              res.data.body.check_cache_responses.map(n => {
                newCheckResult[n.asn_id][n.scheme] = !!n.accessible
              })
              setCheckResult(newCheckResult)
              setMessage({type: "success", text: language.homecheck.success})
            }
          } else {
            clearInterval(checkInterval)
            console.warn(res.data)
            setMessage({type: "warn", text: language.homecheck.warn})
          }
        })
        .catch(err => {
          console.log(err)
          setMessage({type: "error", text: language.homecheck.warn})
        })
    }
  }

  return (
    <div {...rest}>
      <div
        className="py-5"
        css={
          {
            backgroundImage: theme.theme == "dark" ? "linear-gradient(to top, #080e2bcf, rgba(31, 25, 37, 0.87)), url(/background.png)" : "linear-gradient(to top, rgba(33, 150, 243, 0.7), rgba(33, 150, 243, 0.9)), url(/background.png)",
            backgroundSize: "cover"
          }
        }
      >
        <Container>
          <Heading color="#fff" as="h1" css={{ fontSize: 30, maxWidth: 600, margin: "20px auto", textAlign: "center", marginBottom: 5, paddingTop: 100, marginTop: 0 }}>
            {language.homecheck.title}
          </Heading>
          <Paragraph css={{ color: "#fff", opacity: .8, textAlign: "center", marginBottom: 30 }}>
            {language.homecheck.description}
          </Paragraph>
          <Card {...(theme.theme == "light" ? ({ css: { background: "#fff" }}) : {})}>
            {message && (<ResponseMessage type={message.type || null}>{message.text}</ResponseMessage>)}
            <div css={{ display: "grid", gridTemplateColumns: "repeat(3, minmax(100px, 1fr))", gridGap: 15 }}>
              {Object.entries(checkResult).map(([key, value]) => value.HTTP && <CheckResult result={value} className="mb-3" language={language} css={{ background: theme.globalBg}} /> )}
            </div>
            <div className="d-flex">
              <RegistryField
                className="col mr-2 px-4"
                onChange={onFieldChange}
                {...(theme.theme == "light" ? ({ css: { background: theme.darkBg, ...fieldDefaultStyles }}) : fieldDefaultStyles)}
                placeholder={language.homecheck.search_placeholder}
              />
              <Button
                className="d-inline-block"
                css={{ fontSize: 13, padding: "20px 25px" }}
                onClick={onClick}
              >
                {language.homecheck.button_value}
              </Button>
            </div>
          </Card>
        </Container>
      </div>
    </div>
  );
}
