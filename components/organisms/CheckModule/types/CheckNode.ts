export type CheckNode = {
  id?: number,
  asn_id?: number,
  country_code?: string,
  ip?: string,
  isp?: string,
  created_at?: string,
  updated_at?: string
}
