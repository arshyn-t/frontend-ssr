import { Props } from './props';

export * from './component';

export type CheckResultProps = Props;
