export type Record = {
  id: Number,
  reason?: string,
  domain?: string,
  organization?: string,
  doctype?: string,
  number?: Number,
  unlocked?: string
}
