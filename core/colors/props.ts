export type Props = {
  readonly accentBlue: string;
  readonly accentBlueHover: string;
  readonly blockShadow: string;
  readonly globalBg: string;
  readonly darkBg: string;
  readonly lightBg: string;
  readonly textColor: string;
  readonly theme: string;
};
