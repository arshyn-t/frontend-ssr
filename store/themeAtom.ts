import { declareAction, declareAtom } from '@reatom/core';

export const changeTheme = declareAction<string>();
export const themeAtom = declareAtom('dark', on => [on(changeTheme, (state, payload) => payload)]);
